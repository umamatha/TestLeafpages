package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC002_EditLead";
		testCaseDescription ="Edit a lead";
		category = "Smoke";
		author= "Mamatha";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public  void EditLead(String phnNumber, String updateCname)   {
		new MyHomePage()
		.clickLeads()
		.clickFindLead()
		.clickPhonetab()
		.typePhone(phnNumber)
		.clickFindLeadsbutton()
		.selectFirstLead()
		.clickEdit()
		.editCompanyName(updateCname)
		.clickUpdateButton();
		
		
		
		
		
	}
	

}

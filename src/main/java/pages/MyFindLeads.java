package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MyFindLeads extends ProjectMethods{

	
	public MyFindLeads clickPhonetab() {
		WebElement elephone= locateElement("xpath", "//span[text()='Phone']");
		click(elephone);
		return this;
	}
	public MyFindLeads typePhone(String phnNumber ) {
		WebElement elephone= locateElement("name", "phoneNumber");
		type(elephone,phnNumber);
		return this;
	}
	public MyFindLeads clickFindLeadsbutton()   {
		WebElement eleFindLeads= locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public ViewLeadsPage selectFirstLead() 	{
		WebElement eleSelectFirst= locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]");
		click(eleSelectFirst);
		return new ViewLeadsPage();
	}
	
	
}










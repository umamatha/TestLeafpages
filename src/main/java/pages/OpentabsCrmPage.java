package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class OpentabsCrmPage extends ProjectMethods{

	public OpentabsCrmPage editCompanyName(String data) {
		WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		type(eleCompanyName, data);
		return this;
	}

	public ViewLeadsPage clickUpdateButton() {
		WebElement eleUpdateButton = locateElement("class", "smallSubmit");
		click(eleUpdateButton);
		return new ViewLeadsPage();
	}
	
		
}










package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LogInPage extends ProjectMethods{
	public LogInPage()
	{
		PageFactory.initElements(driver, this);
	}
	@CacheLookup
	@FindBy(id="username")
	WebElement eleUserName;
	@CacheLookup
	@FindBy(id="password")
	WebElement elePassword;
	@CacheLookup
	@FindBy(className="decorativeSubmit")
	WebElement eleLogin;
	
	
	
  LogInPage typeUserName(String data) {
	  //
		type(eleUserName, data);
		return this;
	}
	
	public LogInPage typePassword(String data) {
		type(elePassword, data);
		return this;
	}
	
	public HomePage clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}
	
}










package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	@Before
	public void beforecucumber(Scenario sc)
	{
		System.out.println("scenario name :" +sc.getName());
		System.out.println("Scenario ID: "+sc.getId());
	}
	@After
	public void afterScenario(Scenario sc)
	{
		System.out.println("status :" +sc.getStatus());
	}
	

}

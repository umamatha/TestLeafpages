Feature: Login into  Leaftap Application 
Background:
    Given launch the browser 
	And maximize the browser 
	And set the timeouts 
	And enter the URL 

Scenario Outline: Positive flow for login 
	
	And enter the user name as <username> 
	And enter password as <password>
	When clicks on the login button 
	Then verify login success  
		
Examples:
|username|password|
|DemoSalesManager|crmsfa|



Scenario Outline: Negative flow for login
    And enter the user name as <username> 
	And enter password as <password>
	When clicks on the login button 
	But verify login fail 
	
Examples:
|username|password|
|DemoSalesManager1|crmsfa|
|Demo|sales|

	

	
	
